$(function(){
    $t_temperatura = $('#t_temperatura');
    $temperatura = $('#temperatura');
    $t_luminosidade = $('#t_luminosidade');
    $luminosidade = $('#luminosidade');
    $star = $('#star');
    $radius = $('#radius span');
    radius = 0;
    temperatura = 5800;
    luminosidade = 1;
    
    // $temperatura.change(function(){
    //     var new_value = $(this).val();
    //     if($(this).val() < 10000) $(this).attr('step', 100);
    //     else{
    //         $(this).attr('step', 1000);
    //         //se valor atual for > 10000 arredondar
    //         new_value = ($(this).val() * 0.001).toFixed() * 1000;
    //         $(this).val(new_value);
    //     }
    //     $t_temperatura.val(parseInt(new_value)).change();
    // }).change();

    // $luminosidade.change(function(){
    //     $t_luminosidade.val(parseInt($(this).val())).change();
    // }).change();

    // $t_luminosidade.change(function(){
    //     get_data();
    //     calc_radius();
    // });
    // $t_temperatura.change(function(){
    //     get_data();
    //     calc_radius();
    // }).change();
// ############################################################################
    $t_temperatura.change(function(){
        temperatura = $(this).val();
        calc_radius();
    });
    $t_luminosidade.change(function(){
        luminosidade = $(this).val();
        calc_radius();
    }).change();

    $temperatura.slider({
        slide: function(event, ui){
            temperatura = ui.value;
            
            if(temperatura >= 10000){
                temperatura = (temperatura * 0.001).toFixed() * 1000;
                $(this).slider('value', temperatura);
            }
            $t_temperatura.val(parseInt(temperatura)).change();
        },
        value: 5800,
        min: 2300,
        max: 40000,
        step: 100
    });

    $luminosidade.slider({
        slide: function(event, ui){
            luminosidade = Math.pow(10, ui.value);
            luminosidade = (luminosidade < 10) ? luminosidade.toPrecision(2) : Math.round(luminosidade);
            
            $(this).slider('value', luminosidade);
            $t_luminosidade.val(luminosidade).change();
        },
        value: 0,
        min: -4,
        max: 6,
        step: 0.1
    });

});

function calc_radius(){
    radius = Math.sqrt(luminosidade) / ((temperatura / 5800) * (temperatura / 5800));
    $radius.text(radius.toPrecision(2));
    draw();
}

function draw(){
    var new_radius = radius * 150;
    var new_top = 170 - ((new_radius - 150) / 2);
    $star.css({
        width: new_radius,
        height: new_radius,
        'margin-top': new_top
    });
    colorize();
}

function _colorize(){
    var variacao = 37700;
    var start = [255, 158, 48];
    var end = [160, 182, 255];
    var diffs = [variacao / (end[0] - start[0]), variacao / (end[1] - start[1]), variacao / (end[2] - start[2])];
    var new_color = [parseInt(start[0] + (temperatura / diffs[0])),
                    parseInt(start[1] + (temperatura / diffs[1])),
                    parseInt(start[2] + (temperatura / diffs[2]))];

    $star.css('background', 'rgb('+new_color.toString()+')');
}

function colorize(){
    var variacao;
    var diffs;
    var new_color;
    var start = [255, 158, 48];
    var middle = [255,241,229];
    var end = [160, 182, 255];

    if(temperatura == 5800){
        new_color = middle;
    } else if(temperatura < 5800){
        variacao = 5800 - 2300;
        diffs = [variacao / (middle[0] - start[0]), variacao / (middle[1] - start[1]), variacao / (middle[2] - start[2])];
        new_color = [parseInt(start[0] + (temperatura / diffs[0])),
                    parseInt(start[1] + (temperatura / diffs[1])),
                    parseInt(start[2] + (temperatura / diffs[2]))];
    } else if(temperatura > 5800){
        variacao = 40000 - 5800;
        diffs = [variacao / (end[0] - middle[0]), variacao / (end[1] - middle[1]), variacao / (end[2] - middle[2])];
        new_color = [parseInt(middle[0] + (temperatura / diffs[0])),
                    parseInt(middle[1] + (temperatura / diffs[1])),
                    parseInt(middle[2] + (temperatura / diffs[2]))];
    }

    console.log(start);
    console.log(middle);
    console.log(end);
    console.log(diffs);
    console.log(new_color);
    $star.css('background', 'rgb('+new_color.toString()+')');
}